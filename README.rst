SYNOPSIS
--------

ProDy is a free and open-source Python package for protein structure, dynamics,
and sequence analysis.  It allows for comparative analysis and modeling of 
protein structural dynamics and sequence co-evolution.  Fast and flexible ProDy
API is for interactive usage as well as application development.  ProDy also  
comes with several analysis applications and a graphical user interface for 
visual analysis. 


GETTING PRODY
-------------   

You can run ProDy on all major platforms.  For download and installation
instructions see:

* http://www.csb.pitt.edu/ProDy/getprody.html


DOCUMENTATION
-------------

* Homepage: http://www.csb.pitt.edu/ProDy

* Examples: http://www.csb.pitt.edu/ProDy/examples

* Tutorial: http://www.csb.pitt.edu/ProDy/tutorial

* Reference: http://www.csb.pitt.edu/ProDy/reference

* Applications: http://www.csb.pitt.edu/ProDy/apps

* NMWiz GUI: http://www.csb.pitt.edu/NMWiz 

* Changes: http://www.csb.pitt.edu/ProDy/changes.html


SOURCE CODE
-----------

* Source code: https://bitbucket.org/abakan/prody

* Issue tracker: https://bitbucket.org/abakan/prody/issues


LICENSE
-------
  
ProDy is available under GNU General Public License version 3. 
See LICENSE.txt for more details. 

Biopython (http://biopython.org/) KDTree and pairwise2 modules are distributed 
with the ProDy package. Biopython is developed by The Biopython Consortium and 
is available under the Biopython license (http://www.biopython.org/DIST/LICENSE).

The pyparsing (http://pyparsing.wikispaces.com/) module is distributed with 
the ProDy package. Pyparsing is developed by Paul T. McGuire and is available 
under the MIT license (http://www.opensource.org/licenses/mit-license.php).

The argparse module (http://code.google.com/p/argparse/) is distributed with 
the ProDy package. Argparse is developed by Steven J. Bethard and is available 
under the Python Software Foundation License.
