.. _funding:

*******************************************************************************
Funding
*******************************************************************************

Support from NIH grants 1R01GM099738-01 (current) and 1R01GM086238-01 for 
development and maintenance of ProDy is acknowledged by `Ivet Bahar 
<http://www.ccbb.pitt.edu/Faculty/bahar/>`_.

The project described was supported by Grant Number UL1 RR024153 from the 
`National Center for Research Resources <http://www.ncrr.nih.gov/>`_ (NCRR), 
a component of the National Institutes of Health (NIH) and NIH Roadmap for 
Medical Research, and its contents are solely the responsibility of the authors 
and do not necessarily represent the official view of NCRR or NIH.  Information 
on Re-engineering the Clinical Research Enterprise can be obtained from `here 
<http://nihroadmap.nih.gov/clinicalresearch/overview-translational.asp>`_.
