.. _people:

*******************************************************************************
People
*******************************************************************************

ProDy and :ref:`nmwiz` are envisioned, designed, and developed by 
`Ahmet Bakan`_ in `Bahar Lab`_ at the `University of Pittsburgh`_.


`Anindita Dutta`_ contributed to the development of :ref:`evol-apps`,
:mod:`.database` and :mod:`~.prody.sequence` modules.

`Lidio Meireles`_ provided insightful comments on the design of ProDy classes 
and modules, and contributed to the development of early versions of 
:ref:`commands` for automated tasks.

`Ying Liu`_ provided the 
code for Perturbation Response Scanning method.   

`Kian Ho`_ contributed with bug fixes and unit tests for DSSP functions.

.. _Ahmet Bakan: http://www.csb.pitt.edu/people/abakan/
.. _Bahar Lab: http://www.ccbb.pitt.edu/faculty/bahar/
.. _University of Pittsburgh: http://www.pitt.edu/
.. _Anindita Dutta: http://www.linkedin.com/pub/anindita-dutta/5a/568/a90
.. _Lidio Meireles: http://www.linkedin.com/in/lidio
.. _Ying Liu: http://www.linkedin.com/pub/ying-liu/15/48b/5a9
.. _Kian Ho: https://github.com/kianho 
