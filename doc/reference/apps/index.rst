.. _apps:

**********
prody.apps
**********

.. automodule:: prody.apps

.. toctree::
   :maxdepth: 2
   :glob:
   :hidden:

   *
