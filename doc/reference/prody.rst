*****
prody
*****

.. module:: prody


This module defines functions for aiding interactive ProDy experience. 


  * :func:`confProDy`
  * :func:`checkUpdates`
  * :func:`startLogfile`
  * :func:`closeLogfile`
  * :func:`plog`
  * :func:`test`
    
.. autofunction:: confProDy

.. autofunction:: checkUpdates

.. autofunction:: startLogfile

.. autofunction:: closeLogfile

.. autofunction:: plog

.. autofunction:: test
