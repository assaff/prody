.. _sequence:

**************
prody.sequence
**************

.. automodule:: prody.sequence

.. toctree::
   :maxdepth: 2
   :glob:
   :hidden:

   *
